       IDENTIFICATION DIVISION.
       PROGRAM-ID.
            "TEST".
       AUTHOR. Ferran Pegueroles.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
            decimal-point is comma.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

        select optional fitx-asc
             assign disk
             access sequential
             organization sequential
             file status file-status.


       DATA DIVISION.
       FILE SECTION.

       fd  fitx-asc
           value of file-id assign-asc.
        copy 'test2.fd'.

       WORKING-STORAGE SECTION.

        01 file-status   pic xx.
        01 assign-asc    pic x(60) value spaces.
        01 i             pic 9(8).


       PROCEDURE DIVISION.
       TEST-PROGRAM SECTION.
       TEST-PROGRAM-ENTRY.
      * Create simple file
           move 'test-file-3.txt' to assign-asc.
           open output fitx-asc.

           move spaces to file-fd.
           initialize file-fd.
           write file-fd.

           initialize file-fd.
           move 12345           to field-1-1.
           move '������'        to field-2.
           move 12345           to field-3.
           move 12345,78        to field-3.
           move 12345           to field-5.
           move 12345,78        to field-6.
           move 12345           to field-7.
           move 12345,78        to field-8.
           move 'some text1'    to field-9-1(1).
           move 'some text2'    to field-9-1(2).
           move 'some text3'    to field-9-1(3).
           perform varying i from 1 by 1 until i > 10
             move 'some text'   to field-10-1(i)
             move i             to field-10-2(i)
           end-perform.

           move 12345          to field-11.
           move 12345          to field-12.
           write file-fd.


           initialize file-fd.
           move -12435          to field-1-1.
           move '������'        to field-2.
           move -12345          to field-3.
           move -12345,78       to field-3.
           move -12345          to field-5.
           move -12345,78       to field-6.
           move -12345          to field-7.
           move -12345,78       to field-8.
           move 'some text1'    to field-9-1(1).
           move 'some text2'    to field-9-1(2).
           move 'some text3'    to field-9-1(3).
           perform varying i from 1 by 1 until i > 10
             move 'some text'   to field-10-1(i)
             multiply i by -1 giving field-10-2(i)
           end-perform.

           move -12345          to field-11.
           move -12345          to field-12.
           write file-fd.

           close fitx-asc.

      * Create big file

           move 'test-file-4.txt' to assign-asc.
           open output fitx-asc.

           perform varying i from 0 by 1
                                until i > 10000
              initialize file-fd
              move i            to field-1-1 field-3
              compute field-4 = (i * -1,01)
              move field-3      to field-5
              move field-6      to field-6
              move 'test text'  to field-2

              write file-fd
           end-perform.

           close fitx-asc.

       TEST-PROGRAM-END.
           stop run.

