# -*- coding: utf8 -*-
from __future__ import unicode_literals

import locale
import os
import unittest
from decimal import Decimal

from cobutils.extract import edit_value, record_iterator, records
from cobutils.parser import load_definition_from_file

TEST1_FD = os.path.join(os.path.dirname(__file__), 'test1.fd')

TESTFILE1 = os.path.join(os.path.dirname(__file__), 'test-file-1.txt')
TESTFILE3 = os.path.join(os.path.dirname(__file__), 'test-file-3.txt')


EXPECTED = (
    ('field-1-1', 0, 12345, 12435),
    ('field-1-1-1', 0, 0, 0),
    ('field-1-1-2', 0, 12, 12),
    ('field-1-1-3', 0, 3, 4),
    ('field-1-1-4', 0, 45, 35),
    ('field-2',
        b' '.ljust(25),
        'ñññççç'.encode('latin1').ljust(25),
        'ñññççç'.encode('latin1').ljust(25)),
    ('field-3', 0, 12345, -12345),
    ('field-4', 0, 0, 0),
    ('field-5', 0, 12345, -12345),
    ('field-6', 0, Decimal('12345.78'), Decimal('-12345.78')),
    ('field-7', 0, 12345, -12345),  # TODO 12345
    ('field-8', 0, Decimal('12345.78'), Decimal('-12345.78')),  # TODO 12345
    ('field-9-1(1)', b' ' * 10, b'some text1', b'some text1'),
    ('field-9-1(2)', b' ' * 10, b'some text2', b'some text2'),
    ('field-9-1(3)', b' ' * 10, b'some text3', b'some text3'),
    ('field-10-1(1)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(2)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(3)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(4)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(5)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(6)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(7)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(8)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(9)', b' ' * 20, b'some text'.ljust(20),
                                 b'some text'.ljust(20)),
    ('field-10-1(10)', b' ' * 20, b'some text'.ljust(20),
                                b'some text'.ljust(20)),
    ('field-10-2(1)', 0, 1, -1),
    ('field-10-2(2)', 0, 2, -2),
    ('field-10-2(3)', 0, 3, -3),
    ('field-10-2(4)', 0, 4, -4),
    ('field-10-2(5)', 0, 5, -5),
    ('field-10-2(6)', 0, 6, -6),
    ('field-10-2(7)', 0, 7, -7),
    ('field-10-2(8)', 0, 8, -8),
    ('field-10-2(9)', 0, 9, -9),
    ('field-10-2(10)', 0, 10, -10),
    ('field-11', 0, 12345, -12345),
    ('field-12', 0, 12345, -12345),
    ('filler', b' ' * 25, b' '.ljust(25), b' '.ljust(25)),
    ('field-14', 0, 0, 0),
)


class TestExtractLineSecuential(unittest.TestCase):

    LINE_SEQUENTIAL = True
    TEST_FILE = TESTFILE1

    def setUp(self):
        self.definition = load_definition_from_file(TEST1_FD)
        self.datain = open(self.TEST_FILE, 'rb')
        self.it = record_iterator(self.definition, self.datain,
            self.LINE_SEQUENTIAL)

    def test_extract_row_count(self):
        row_count = len([row for row in self.it])
        self.assertEqual(row_count, 3)

    def test_extract_is_dict(self):
        row1 = next(self.it)
        self.assertTrue(isinstance(row1, dict))

    def test_extract_row_fields_count(self):
        rows = [row for row in self.it]

        self.assertEqual(len(rows[0].keys()), len(EXPECTED))

    def test_extract_field_names(self):
        rows = [row for row in self.it]

        keys = sorted(rows[0].keys())
        sort_expected = sorted(EXPECTED)
        for key, expected in zip(keys, sort_expected):
            name = expected[0]
            self.assertEqual(key, name)

    def test_extract_field_value_occurs(self):
        row = next(self.it)

        self.assertEqual(row['field-10-1(1)'], b' ' * 20)
        self.assertEqual(row['field-10-2(1)'], 0)
        self.assertEqual(row['field-10-1(2)'], b' ' * 20)
        self.assertEqual(row['field-10-2(2)'], 0)
        self.assertEqual(row['field-10-1(10)'], b' ' * 20)
        self.assertEqual(row['field-10-2(10)'], 0)

        row = next(self.it)
        self.assertEqual(row['field-10-1(1)'], b'some text'.ljust(20))
        self.assertEqual(row['field-10-2(1)'], Decimal(1))
        self.assertEqual(row['field-10-1(2)'], b'some text'.ljust(20))
        self.assertEqual(row['field-10-2(2)'], Decimal(2))

    def test_extract_field_values_0(self):
        row = next(self.it)

        for expected in EXPECTED:
            name = expected[0]
            self.assertEqual(row[name], expected[1])

    def test_extract_field_values_1(self):
        row = next(self.it)
        row = next(self.it)

        for expected in EXPECTED:
            name = expected[0]
            self.assertEqual(row[name], expected[2])

    def test_extract_field_values_2(self):
        row = next(self.it)
        row = next(self.it)
        row = next(self.it)
        for expected in EXPECTED:
            name = expected[0]
            self.assertEqual(row[name], expected[3])


class TestExtractSecuential(TestExtractLineSecuential):

    LINE_SEQUENTIAL = False
    TEST_FILE = TESTFILE3


class TestExtractSecuentialNumRows(unittest.TestCase):

    LINE_SEQUENTIAL = False
    TEST_FILE = TESTFILE3

    def setUp(self):
        self.definition = load_definition_from_file(TEST1_FD)
        self.datain = open(self.TEST_FILE, 'rb')
        self.records = records(self.definition, self.datain,
            self.LINE_SEQUENTIAL)

    def test_extract_row_count(self):
        self.assertEqual(len(self.records), 3)

    def test_extract_row_count_2(self):
        datain = open(self.TEST_FILE, 'rb')
        it = record_iterator(self.definition, datain,
            self.LINE_SEQUENTIAL)

        row_count = len([row for row in it])
        self.assertEqual(row_count, 3)


class TestEditValue(unittest.TestCase):

    def test_str(self):
        self.assertEqual(edit_value(b'abc'), b"abc")

    def test_str2(self):
        self.assertEqual(edit_value(b'abc   '), b"abc")

    def test_unicode(self):
        self.assertEqual(edit_value(u'abc'), b"abc")

    def test_unicode2(self):
        self.assertEqual(edit_value(u'àñ'), b'\xe0\xf1')

    def test_decimal(self):
        self.assertEqual(edit_value(Decimal("1234.56")),
                            locale.str(Decimal("1234.56")))

    def test_float(self):
        self.assertEqual(edit_value(1.23), locale.str(1.23))

    def test_other(self):
        self.assertEqual(edit_value(12345), 12345)
