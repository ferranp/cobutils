# -*- coding: utf8 -*-
import os
import tempfile
import unittest

from cobutils.extract import records
from cobutils.parser import load_definition_from_file

TESTFILE1 = os.path.join(os.path.dirname(__file__), 'test-file-1.txt')
TESTFILE2 = os.path.join(os.path.dirname(__file__), 'test-file-2.txt')
TESTFILE3 = os.path.join(os.path.dirname(__file__), 'test-file-3.txt')
TESTFILE4 = os.path.join(os.path.dirname(__file__), 'test-file-4.txt')

TEST2_FD = os.path.join(os.path.dirname(__file__), 'test2.fd')


class TestCreateSimple(unittest.TestCase):

    def setUp(self):
        self.definition = load_definition_from_file(TEST2_FD)
        self.datain = open(TESTFILE1, "rb").readlines()
        self.records = records(self.definition, self.datain, True)

    def test_create_str(self):
        record = self.records[0]
        record_bytes = record.get_bytes()
        self.assertTrue(isinstance(record_bytes, type(b'')))

    def test_create_record_length_0(self):
        record = self.records[0]
        record_bytes = record.get_bytes()
        self.assertEqual(len(record_bytes), self.definition.size())

    def test_create_record_length_2(self):
        record = self.records[2]
        record_bytes = record.get_bytes()
        self.assertEqual(len(record_bytes), len(self.datain[2].strip(b'\r\n')))

    # disabled
    def test_create_one_field(self):
        record = self.records[0]
        field = self.definition.children_map['field-2']
        self.assertEqual(field.get_bytes(record), b' '.ljust(25))

    def test_create_record_0(self):
        record = self.records[0]
        data = self.datain[0].strip(b'\r\n')

        record_bytes = record.get_bytes()

        self.assertEqual(record_bytes[0:100], data[0:100])
        self.assertEqual(record_bytes[100:200], data[100:200])
        self.assertEqual(record_bytes[200:300], data[200:300])
        self.assertEqual(record_bytes, data)

    def test_create_record_1(self):
        record = self.records[1]
        data = self.datain[1].strip(b'\r\n')

        record_bytes = record.get_bytes()

        self.assertEqual(record_bytes[0:20], data[0:20])
        self.assertEqual(record_bytes[20:40], data[20:40])
        self.assertEqual(record_bytes[40:80], data[40:80])
        self.assertEqual(record_bytes[80:100], data[80:100])
        self.assertEqual(record_bytes[0:100], data[0:100])
        self.assertEqual(record_bytes[100:200], data[100:200])
        self.assertEqual(record_bytes[200:300], data[200:300])
        self.assertEqual(record_bytes, data)

    def test_create_record_2(self):
        record = self.records[2]
        data = self.datain[2].strip(b'\r\n')

        record_bytes = record.get_bytes()

        self.assertEqual(record_bytes[0:100], data[0:100])
        self.assertEqual(record_bytes[100:200], data[100:200])
        self.assertEqual(record_bytes[200:300], data[200:300])
        self.assertEqual(record_bytes, data)


class TestExtractCreate(unittest.TestCase):

    FILE = TESTFILE1
    FD = TEST2_FD
    LINE_SEQUENTIAL = True
    RECORDS = 3

    def setUp(self):
        self.definition = load_definition_from_file(self.FD)
        self.datain = open(self.FILE, 'rb')
        self.records = records(self.definition, self.datain,
                    self.LINE_SEQUENTIAL)

        handler, self.filename = tempfile.mkstemp()
        self.dataout = os.fdopen(handler, 'wb')

    def tearDown(self):
        self.dataout.close()
        os.unlink(self.filename)

    def test_records(self):
        self.assertEqual(len(self.records), self.RECORDS)

    def test_extract_create(self):
        for record in self.records:
            data = record.get_bytes()
            self.dataout.write(data)
            if self.LINE_SEQUENTIAL:
                self.dataout.write(b'\r\n')

        self.dataout.close()

        created = open(self.filename, 'rb').read()
        original = open(self.FILE, 'rb').read()

        self.assertEqual(len(original), len(created))
        self.assertEqual(original, created)


class TestExtractCreate2(TestExtractCreate):

    FILE = TESTFILE3
    FD = TEST2_FD
    LINE_SEQUENTIAL = False
    RECORDS = 3


#class TestExtractCreate3(TestExtractCreate):
#
#    FILE = TESTFILE2
#    FD = TEST2_FD
#    LINE_SEQUENTIAL = True
#    RECORDS = 10001
