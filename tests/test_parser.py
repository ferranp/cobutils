import os
import unittest

from cobutils.parser import RecordParser, load_definition_from_file

TEST_FD = os.path.join(os.path.dirname(__file__), 'test.fd')

"""
01 file-fd.
   02 field-1.
      04 field-1-1            pic 9(09).
      04 rfield-1-1   redefines field-1-1.
         05 field-1-1-1       pic 9(4).
         05 field-1-1-2       pic 99.
         05 field-1-1-3       pic 9.
         05 field-1-1-4       pic 99.
   02 field-2                 pic x(25).
   02 field-3                 pic s9(7).
   02 field-4                 pic s9(5)v99.
   02 field-5                 pic s9(7)    comp-3.
   02 field-6                 pic s9(5)v99 comp-3.
   02 field-7                 pic s9(7)    comp.
   02 field-8                 pic s9(5)v99 comp.

   02 field-9.
      03 field-9-1            pic x(10) occurs 3.

   02 field-10 occurs 10.
      03 field-10-1           pic x(20).
      03 field-10-2           pic s9(5).

   02 field-11                pic s9(7)
                              sign trailing separate.
   02 field-12                pic s9(10)
                              sign leading separate.
   02                         pic x(25).
   02 field-14                pic s9(10)
                              sign leading separate.
   02 field-15                pic +9(10).
   02 field-16                pic 9999,999.
   02 field-17                pic ZZZ.ZZ9,99.
   02 field-18                pic ---.--9,99.
"""

EXPECTED = (
    (1, 'file-fd', None, 0, None),
    (2, 'field-1', None, 0, None),
    (4, 'field-1-1', ['9(09)'], 0, None),
    (4, 'rfield-1-1', None, 0, 'field-1-1'),
    (5, 'field-1-1-1', ['9(4)'], 0, None),
    (5, 'field-1-1-2', ['99'], 0, None),
    (5, 'field-1-1-3', ['9'], 0, None),
    (5, 'field-1-1-4', ['99', 'display'], 0, None),
    (2, 'field-2', ['x(25)'], 0, None),
    (2, 'field-3', ['s9(7)'], 0, None),
    (2, 'field-4', ['s9(5)v99'], 0, None),
    (2, 'field-5', ['s9(7)', 'comp-3'], 0, None),
    (2, 'field-6', ['s9(5)v99', 'comp-3'], 0, None),
    (2, 'field-7', ['s9(7)', 'comp'], 0, None),
    (2, 'field-8', ['s9(5)v99', 'comp'], 0, None),
    (2, 'field-9', None, 0, None),
    (3, 'field-9-1', ['x(10)'], 3, None),
    (2, 'field-10', None, 10, None),
    (3, 'field-10-1', ['x(20)'], 0, None),
    (3, 'field-10-2', ['s9(5)'], 0, None),
    (2, 'field-11', ['s9(7)', 'sign', 'trailing', 'separate'], 0, None),
    (2, 'field-12', ['s9(10)', 'sign', 'leading', 'separate'], 0, None),
    (2, 'filler', ['x(25)'], 0, None),
    (2, 'field-14', ['s9(10)', 'sign', 'leading', 'separate'], 0, None),
    (2, 'field-15', ['+9(10)'], 0, None),
    (2, 'field-16', ['9999,999'], 0, None),
    (2, 'field-17', ['zzz.zz9,99'], 0, None),
    (2, 'field-18', ['---.--9,99'], 0, None),
)


class TestFDParser(unittest.TestCase):

    def test_parse_file(self):
        record_parser = RecordParser(TEST_FD)
        fields = list(record_parser)
        self.assertEqual(len(fields), len(EXPECTED))


class TestRecordParser(unittest.TestCase):
    def setUp(self):
        record_parser = RecordParser(TEST_FD)
        self.fields = list(record_parser)

    def test_parse_fields(self):
        for i, field in enumerate(self.fields):
            expected = EXPECTED[i]
            self.assertEqual(expected, field)


TEST1_ERROR_FD = os.path.join(os.path.dirname(__file__), 'test1-error.fd')


class TestErrorParser(unittest.TestCase):

    def test_parse_fields(self):
        record_parser = RecordParser(TEST1_ERROR_FD)
        self.assertRaises(Exception, lambda: list(record_parser))


class TestDefinitionParser(unittest.TestCase):

    def test_load_definition_from_file(self):
        definition = load_definition_from_file(TEST_FD)
        self.assertEqual(definition.level, 1)
        self.assertEqual(definition.name, 'file-fd')
