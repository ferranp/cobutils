import unittest

from cobutils.parser import load_definition_from_file

from .test_parser import TEST_FD

SQL_FIELDS = (
    ('field_1_1', 'numeric(9,0)'),
    ('field_2', 'varchar(25)'),
    ('field_3', 'numeric(7,0)'),
    ('field_4', 'numeric(7,2)'),
    ('field_5', 'numeric(7,0)'),
    ('field_6', 'numeric(7,2)'),
    ('field_7', 'numeric(7,0)'),
    ('field_8', 'numeric(7,2)'),
    ('field_9_1_1', 'varchar(10)'),
    ('field_9_1_2', 'varchar(10)'),
    ('field_9_1_3', 'varchar(10)'),
    ('field_10_1_1', 'varchar(20)'),
    ('field_10_2_1', 'numeric(5,0)'),
    ('field_10_1_2', 'varchar(20)'),
    ('field_10_2_2', 'numeric(5,0)'),
    ('field_10_1_3', 'varchar(20)'),
    ('field_10_2_3', 'numeric(5,0)'),
    ('field_10_1_4', 'varchar(20)'),
    ('field_10_2_4', 'numeric(5,0)'),
    ('field_10_1_5', 'varchar(20)'),
    ('field_10_2_5', 'numeric(5,0)'),
    ('field_10_1_6', 'varchar(20)'),
    ('field_10_2_6',  'numeric(5,0)'),
    ('field_10_1_7', 'varchar(20)'),
    ('field_10_2_7', 'numeric(5,0)'),
    ('field_10_1_8', 'varchar(20)'),
    ('field_10_2_8', 'numeric(5,0)'),
    ('field_10_1_9', 'varchar(20)'),
    ('field_10_2_9', 'numeric(5,0)'),
    ('field_10_1_10', 'varchar(20)'),
    ('field_10_2_10', 'numeric(5,0)'),
    ('field_11', 'numeric(7,0)'),
    ('field_12', 'numeric(10,0)'),
    ('filler', 'varchar(25)'),
    ('field_14', 'numeric(10,0)'),
    ('field_15', 'varchar(6)'),
    ('field_16', 'varchar(8)'),
    ('field_17', 'varchar(10)'),
    ('field_18', 'varchar(10)'),
)


class TestDefinitionParser(unittest.TestCase):

    def setUp(self):
        self.definition = load_definition_from_file(TEST_FD)

    def test_sql_field_count(self):

        count = len(self.definition.get_sql_fields())

        self.assertEqual(count, len(SQL_FIELDS))

    def test_sql_fieldnames(self):

        fieldnames = [f[0] for f in self.definition.get_sql_fields()]

        for i, name in enumerate(fieldnames):
            self.assertEqual(name, SQL_FIELDS[i][0])

    def test_sql_fieldtypes(self):

        fieldtypes = [f[1] for f in self.definition.get_sql_fields()]

        for i, name in enumerate(fieldtypes):
            self.assertEqual(name, SQL_FIELDS[i][1])
