#!/usr/bin/python

import sys
from optparse import OptionParser

import cobutils


def main():
    usage = """usage: %prog [OPTIONS] [CSVFILE]
Convert a cobol record definition to a SQL table"""
    parser = OptionParser(usage=usage)
    parser.add_option("-r", "--reg", dest="regfilename",
                      help="read register from FILE", metavar="FILE")
    parser.add_option("-t", "--table", dest="table",
                      help="name of the table", metavar="TABLE")
    parser.add_option("-o", "--out", dest="outfilename",
                      help="output FILE", metavar="FILE")

    (options, args) = parser.parse_args()

    if not options.regfilename:
        parser.error("Record definition [-r|--reg] is required")

    if not options.table:
        parser.error("Table name [-t|--table] is required")

    if options.outfilename:
        outfile = open(options.outfilename, 'w')
    else:
        outfile = sys.stdout

    if len(args) != 0:
        parser.error("Bad number of arguments")

    struct = cobutils.load_definition_from_file(options.regfilename)

    sql = "create table %s (\n%s);\n" % \
            (options.table, ",\n".join(struct.get_sql()))

    outfile.write(sql)
    outfile.close()


if __name__ == '__main__':  # pragma no cover
    main()
