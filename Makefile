
VIRTUALENVPATH := .venv


.PHONY: test
test: $(VIRTUALENVPATH)
	# $(MAKE) -C tests all
	. $(VIRTUALENVPATH)/bin/activate ; nosetests -x --with-coverage --cover-package=cobutils
	. $(VIRTUALENVPATH)/bin/activate ; coverage html --include=*.py,cobutils/*py,tests/*py


.PHONY: tox
tox: $(VIRTUALENVPATH)
	. $(VIRTUALENVPATH)/bin/activate ; tox

$(VIRTUALENVPATH): requirements-dev.txt
	virtualenv $@
	. $(VIRTUALENVPATH)/bin/activate ; pip install -r requirements-dev.txt
	touch $@
